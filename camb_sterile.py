"""
cosmosis template
"""
from numpy import log, pi, fabs, exp, arange
from cosmosis.datablock import names as section_names
from cosmosis.datablock import option_section
from camb import CAMBparams, get_results
#from camb import model, initialpower, CAMBparams

cosmo = section_names.cosmological_parameters
cmb = section_names.cmb_cl
matter_powspec = section_names.matter_power_lin

def setup(options):
	return 0

def execute(block, config):
	
	h0  =  block[cosmo, 'h0']
	omch2 = block[cosmo, 'omch2']
	n_s  = block[cosmo, 'n_s']
	log1e10as  = block[cosmo, 'log1e10as']
	ombh2 = block[cosmo, 'ombh2']
	tau = block[cosmo, 'tau']
	omega_k  =  block[cosmo, 'omega_k']
	w  =  block[cosmo, 'w']
	wa  =  block[cosmo, 'wa']
	massless_nu = block[cosmo, 'massless_nu']
	massive_nu = block[cosmo, 'massive_nu']
	mnu_sum = block[cosmo, 'mnu_sum']
	m_eff_sterile = block[cosmo, 'm_eff_sterile']
	sterile_neutrino = block[cosmo, 'sterile_neutrino']
	delta_neff =  block[cosmo, 'delta_neff']

	H0 = 100*h0

	pars = CAMBparams()

	nnu = massless_nu + massive_nu + delta_neff
	standard_neutrino_neff = massless_nu + massive_nu
	As = exp(log1e10as)*(10**-10)

	pars.set_cosmology(H0=H0, ombh2=ombh2, omch2 = omch2, mnu=mnu_sum, omk=omega_k, tau=tau, 
		nnu = nnu, meffsterile = m_eff_sterile, num_massive_neutrinos = massive_nu, 
		neutrino_hierarchy = 'degenerate',
		standard_neutrino_neff = standard_neutrino_neff, tau_neutron=880.3)
	pars.InitPower.set_params(As=As, ns=n_s, nrun=0,)
	pars.set_accuracy(AccuracyBoost=2.0, lSampleBoost=1.0, lAccuracyBoost=1.0, HighAccuracyDefault=True)#, DoLateRadTruncation=True)
	pars.set_for_lmax(2550)
	results = get_results(pars)
	cmb_ps =results.get_cmb_power_spectra(pars, lmax = 2550)
	CL = cmb_ps['lensed_scalar']
	ell = arange(CL.shape[0])
	CL_TT = 7.428*1e12*CL[:,0]

	block[cmb, 'TT'] = CL_TT
	block[cmb, 'ell'] = ell


	#signal that everything went fine
	return 0

def cleanup(config):
	#nothing to do here!  We just include this 
	# for completeness
	return 0
