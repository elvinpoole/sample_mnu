"""
allows m_eff_sterile and mnu_sum to be given as input values
is a simplified version of sample_mnu.py without the 
mixing angle and mass splitting capabilities
"""

from numpy import log, pi
from cosmosis.datablock import names as section_names
from cosmosis.datablock import option_section

cosmo = section_names.cosmological_parameters
cmb = section_names.cmb_cl
matter_powspec = section_names.matter_power_lin

def setup(options):
	return 0

def execute(block, config):
	
	# Get mass parameters from sampler
	mnu_sum = block[cosmo, 'mnu_sum']
	m_eff_sterile = block[cosmo, 'm_eff_sterile']
	h0 = block[cosmo, 'h0']

	#calculate omega_nu
	omega_nu = (mnu_sum + m_eff_sterile)/(94.1* (h0**2.))
	#calculate sterile_mass_fraction
	sterile_mass_fraction = m_eff_sterile / (mnu_sum+m_eff_sterile)
	print "omega_nu    {0}".format(omega_nu)
	print "sterile_mass_fraction    {0}".format(sterile_mass_fraction)

	#save to block
	block[cosmo, 'omega_nu'] = omega_nu
	block[cosmo, 'sterile_mass_fraction'] = sterile_mass_fraction
	

	#signal that everything went fine
	return 0

def cleanup(config):
	#nothing to do here!  We just include this 
	# for completeness
	return 0
