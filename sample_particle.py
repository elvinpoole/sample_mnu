"""
functions for sterile_mnu.py
"""
import numpy as np 
import scipy.interpolate as interp
import os
filedir = os.path.dirname(os.path.realpath(__file__))
matrixdir = filedir + "/testdata/interpmatrix/"

def particle2cosmology(logsin22theta,logdm2,masstype='thermal',m1=0.0,interpmethod2d='cubic',checkresults=True):
    """
    Function to convert one point in particle space to cosmology space
    uses LASAGNA from matrixdir for interpolation
    for use in cosmosis module
    """
    
    #load Neff matrix (LASAGNA output, run17)
    deltaNeff, theta_vec, m_vec = loadinterpmatrix(uselowtheta=True)
    
    #interpolate a dN value for each point in particle space
    neff_flat = deltaNeff.flatten()
    x = np.tile(theta_vec,len(m_vec))
    y = np.repeat(m_vec, len(theta_vec))
    
    dN = interp.griddata((x,y),neff_flat,([logsin22theta],[logdm2]),method=interpmethod2d)[0]
    if np.isnan(dN) == True:
        print 'nan '*15
        dN = interp.griddata((x,y),neff_flat,([logsin22theta],[logdm2]),method='nearest')[0]
    
    m4 = np.sqrt(10**logdm2 + m1**2.)
    if masstype == 'thermal':
        meff = m4 * pow(dN,3./4.)
    elif masstype == 'dw':
        meff = m4*dN
    else:
        raise RuntimeError('Please specify a masstype from: "thermal", "dw". (default = "thermal")')

    #check results are valid
    if checkresults == True:
        if dN > 1.0:
            dN = 1.
        if dN < 0.0:
            dN = 0.
        if meff < 0.:
            meff = 0.

    return dN, meff

def loadinterpmatrix(uselowtheta=True):
    """
    loads the interpolation matrix
    """

    deltaNeff = np.flipud(np.loadtxt(matrixdir + "deltaNeff.txt", unpack=True).T) #deltaNeff[m_index][theta_index]
    m_vec = np.loadtxt(matrixdir + "logdelta_m2_vec.txt") #log dm^2 values
    m_vec = m_vec[::-1] #reverses the m vector
    theta_vec = np.loadtxt(matrixdir + "logsinsq_theta_vec.txt") # log sin^2 2theta values

    if uselowtheta==True:
        deltaNeff2 = np.fliplr(np.flipud(np.loadtxt(matrixdir + "lowtheta/" + "deltaNeff.txt"))) #deltaNeff[m_index][theta_index]
        #m_vec2 = np.loadtxt(matrixdir + "lowtheta/" + "logdm2_vec.txt") #log dm^2 values
        #m_vec2 = m_vec[::-1] #reverses the m vector
        theta_vec2 = np.loadtxt(matrixdir + "lowtheta/" + "theta_vec.txt") # log sin^2 2theta values
        ####
        # add to the current matrix
        ####
        deltaNeff = np.hstack((deltaNeff2,deltaNeff))
        theta_vec = np.append(theta_vec2,theta_vec)

    return deltaNeff, theta_vec, m_vec
