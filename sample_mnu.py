from numpy import log, pi, log10, inf, array
from cosmosis.datablock import names as section_names
from cosmosis.datablock import option_section
import sample_particle as particle

cosmo = section_names.cosmological_parameters
cmb = section_names.cmb_cl
matter_powspec = section_names.matter_power_lin

def setup(options):

	return 0

def execute(block, config):
	
	h0 = block[cosmo, 'h0']
	if block.has_value(cosmo,'mnu_sum') == False:
		print 'WARNING sample mnu is being skipped'
		sterile_mass_fraction = block[cosmo, 'sterile_mass_fraction']

	else:
		mnu_sum = block[cosmo, 'mnu_sum']

		# Get mass parameters from sampler
		cosmo_space = block.has_value(cosmo, 'delta_neff') and block.has_value(cosmo, 'm_eff_sterile')
		osc_log_space = block.has_value(cosmo, 'logdm2') and block.has_value(cosmo, 'logsin22theta')
		osc_lin_space = block.has_value(cosmo, 'dm2') and block.has_value(cosmo, 'sin22theta')
		#to use this module, one of the above combinations must be specified
		assert sum(array([cosmo_space, osc_log_space, osc_lin_space]).astype('int')) == 1

		if cosmo_space == True:
			m_eff_sterile = block[cosmo, 'm_eff_sterile']
		else:
			if osc_log_space == True:
				logdm2 = block[cosmo, 'logdm2']
				logsin22theta = block[cosmo, 'logsin22theta']
			elif osc_lin_space == True:
				logdm2 = log10(block[cosmo, 'dm2'])
				logsin22theta = log10(block[cosmo, 'sin22theta'])

			if logdm2 == -inf and logsin22theta == -inf:
				dN = 0.
				m_eff_sterile = 0.
			else:
				dN, m_eff_sterile = particle.particle2cosmology(logsin22theta,logdm2)
			block[cosmo,'delta_neff'] = dN
			print "dN    {0}".format(dN)
			print "m_eff_sterile    {0}".format(m_eff_sterile)

		#calculate omega_nu
		omega_nu = (mnu_sum + m_eff_sterile)/(94.1* (h0**2.))
		#calculate sterile_mass_fraction
		if mnu_sum == 0. and m_eff_sterile == 0.:
			pass
		else:
			sterile_mass_fraction = m_eff_sterile / (mnu_sum+m_eff_sterile)
			print "sterile_mass_fraction    {0}".format(sterile_mass_fraction)
			block[cosmo, 'sterile_mass_fraction'] = sterile_mass_fraction

			if sterile_mass_fraction == 0.:
				block[cosmo, 'sterile_neutrino'] = 0
				print 'WARNING. sterile_neutrino set to 0'
		print "omega_nu    {0}".format(omega_nu)

		#save to block
		block[cosmo, 'omega_nu'] = omega_nu
	

	#signal that everything went fine
	return 0

def cleanup(config):
	#nothing to do here!  We just include this 
	# for completeness
	return 0
